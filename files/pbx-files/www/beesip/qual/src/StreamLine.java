public class StreamLine {

    private String ip_src = "";
    private int port_src = 0;
    private String ip_dst= "";
    private int port_dst = 0;
    private String SSRC = "";
    private String payload = "";
    private double lost = 0.0;
    private double quality_R = 0.0;
    private double quality_MOS = 0.0;
    private boolean merged = false;

    public StreamLine() {
    }
    
    public StreamLine(String ip_src, int port_src, String ip_dst, int port_dst, String SSRC, String payload, double lost, double quality_R, double quality_MOS, boolean merged) {
        this.ip_src = ip_src;
        this.ip_dst = ip_dst;
        this.port_src = port_src;
        this.port_dst = port_dst;
        this.SSRC = SSRC;
        this.payload = payload;
        this.lost = lost;
        this.quality_R = quality_R;
        this.quality_MOS = quality_MOS;
        this.merged = merged;
    }

    public void setSSRC(String SSRC) {
        this.SSRC = SSRC;
    }

    public void setIp_dst(String ip_dst) {
        this.ip_dst = ip_dst;
    }

    public void setIp_src(String ip_src) {
        this.ip_src = ip_src;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public void setLost(String lost) {
        int pos = lost.indexOf("%");
        String lost_p = lost.substring(1, pos);
        this.lost = new Double(lost_p);
    }

    public void setPort_dst(String port_dst) {
        this.port_dst = new Integer(port_dst);
    }

    public void setPort_src(String port_src) {
        this.port_src = new Integer(port_src);
    }

    public void setQualityR(double quality) {
        this.quality_R = quality;
    }
    
    public void setQualityMOS(double quality) {
        this.quality_MOS = quality;
    }
    
    public void setMerged(boolean merged) {
        this.merged = merged;
    }
    
    public boolean getMerged() {
        return this.merged;
    }
    
    public double getQualityR() {
        return quality_R;
    }
    
    public double getQualityMOS() {
        return quality_MOS;
    }

    public String getSSRC() {
        return SSRC;
    }

    public String getIp_dst() {
        return ip_dst;
    }

    public String getIp_src() {
        return ip_src;
    }

    public double getLost() {
        return lost;
    }

    public String getPayload() {
        return payload;
    }

    public int getPort_dst() {
        return port_dst;
    }

    public int getPort_src() {
        return port_src;
    }
}
