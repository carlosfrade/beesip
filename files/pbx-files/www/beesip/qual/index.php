<?php

$ip = fopen("_ip.log","w");
fwrite($ip, $_SERVER['HTTP_HOST']);
fclose($ip);
#echo $ip;

$file = fopen("_status.log", "r");
$status = fread($file, 1);
fclose($file);

echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";
echo "<html>";

echo "<head>";
  echo "<title>BESIP Monitoring</title>";
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-2\">";
  echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"_style.css\">";
  echo "<script type=\"text/javascript\" src=\"jquery.js\"></script>";
echo "</head>";

echo "<body>";

# *** Status bar for indicating of monitoring status ***
echo "<div id=\"status_bar\">";
  if ($status == 1) {
    echo "<div class=\"text_top\">";
      echo "Monitoring is running...";
    echo "</div>";
  } else {
    echo "<div class=\"text_top\">";
      echo "Monitoring is stopped.";
    echo "</div>";
  }
echo "</div>";

# *** Menu buttons ***
echo "<div id=\"button_bar\">";

if ($status == 0) {
  echo "<div class=\"start\">";
    echo "<div class=\"button\">";
      echo "Start";
    echo "</div>";
  echo "</div>";
} else {
  echo "<div class=\"stop\">";
    echo "<div class=\"button\">";
      echo "Stop";
    echo "</div>";
  echo "</div>";
}

echo "<div class=\"perform\">";
  echo "<div class=\"button\">";
    echo "Results";
  echo "</div>";
echo "</div>";

echo "<div class=\"refresh\">";
  echo "<div class=\"button\">";
    echo "Refresh";
  echo "</div>";
echo "</div>";

echo "<div class=\"erase\">";
  echo "<div class=\"button\">";
    echo "Erase";
  echo "</div>";
echo "</div>";

echo "</div>";

# *** Database reading ***
$db = new SQLite3("qual_res.db"); 
$query = $db->query("SELECT * FROM quality"); 

echo "<div id=\"display_bar\">";

echo "<table align=\"center\" cellpadding=\"10\">";
echo "<tr align=\"center\">";
    echo "<td><div class=\"text_other\">From</div></td>";
    echo "<td><div class=\"text_other\">To</div></td>";
    echo "<td><div class=\"text_other\">R-factor</div></td>";
    echo "<td><div class=\"text_other\">MOS</div></td>";
    echo "<td><div class=\"text_other\">Codec(s)</div></td>";
  
echo "</tr>";

while ( ($row = $query->fetchArray())) 
{ 
echo "<tr align=\"center\">";

  echo "<td>";
  echo $row['q_from'];
  echo "</td>";
  echo "<td>"; 
  echo $row['q_to'];
  echo "</td>";
  echo "<td>";
  echo $row['q_r'];
  echo "</td>";
  echo "<td>";
  echo $row['q_mos'];
  echo "</td>";
  echo "<td>";
  echo  $row['q_codec'];
  echo "</td>"; 

echo "</tr>";
}
echo "</table>"; 

echo "</div>";

$db->close();

echo "<div id=\"footer_bar\">";

echo "</div>";

# *** AJAX functions for button event handling ***
echo "<script>";
echo  "$('.start').click(function() {";
echo    "$.ajax({";
echo      "type: \"GET\",";
echo      "url: \"_start.php\",";
echo        "})";
echo  "});";

echo  "$('.start').click(function() {";
echo    "location.reload()";
echo  "});";

echo  "$('.stop').click(function() {";
echo    "$.ajax({";
echo      "type: \"GET\",";
echo      "url: \"_stop.php\",";
echo        "})";
echo  "});";

echo  "$('.stop').click(function() {";
echo    "location.reload()";
echo  "});";

echo  "$('.perform').click(function() {";
echo    "$.ajax({";
echo      "type: \"POST\",";
echo      "url: \"_perform.php\",";
echo        "})";
echo  "});";

echo  "$('.refresh').click(function() {";
echo    "location.reload()";
echo  "});";

echo  "$('.erase').click(function() {";
echo    "$.ajax({";
echo      "type: \"POST\",";
echo      "url: \"_erase.php\",";
echo        "})";
echo  "});";

echo "</script>";

echo "</body>";

echo "</html>";
 
?>