ifeq ($(ASTPKG),)
 ifeq ($(OWRT_NAME),trunk)
	ASTPKG=asterisk11
 else
	ASTPKG=asterisk11
 endif
endif

ifeq ($(KAMPKG),)
  KAMPKG=kamailio4
endif


BEESIP_LITE_DEPS += \
  +$(ASTPKG) \
  +sqlite3-cli +zabbix-uci-agentd +zabbix-uci-extra-owrt +zabbix-uci-sender \
  +lighttpd +lighttpd-mod-cgi +lighttpd-mod-fastcgi +lighttpd-mod-alias +lighttpd-mod-auth +lighttpd-mod-redirect +lighttpd-mod-rewrite +lighttpd-mod-access +lighttpd-mod-proxy \
  +openssl-util +luci +wget +kmod-ipv6 +kmod-ip6tables +ip6tables \
  +uciprov +curl +$(KAMPKG) +$(KAMPKG)-mod-corex +$(KAMPKG)-mod-auth +$(KAMPKG)-mod-cfg-rpc +$(KAMPKG)-mod-cfgutils +$(KAMPKG)-mod-ctl +$(KAMPKG)-mod-db-sqlite +$(KAMPKG)-mod-exec \
  +$(KAMPKG)-mod-enum +$(KAMPKG)-mod-ipops +$(KAMPKG)-mod-dialog +$(KAMPKG)-mod-nathelper +$(KAMPKG)-mod-pike +$(KAMPKG)-mod-rr +$(KAMPKG)-mod-tm +$(KAMPKG)-mod-usrloc +$(KAMPKG)-mod-rtpproxy \
  +$(KAMPKG)-mod-sipcapture +$(KAMPKG)-mod-siptrace 
 
BEESIP_DEPS += \
  +$(KAMPKG)-mod-acc  +$(KAMPKG)-mod-alias-db \
  +$(KAMPKG)-mod-db-flatstore \
  +$(KAMPKG)-mod-db-text +$(KAMPKG)-mod-dialplan \
  +$(KAMPKG)-mod-diversion +$(KAMPKG)-mod-domain +$(KAMPKG)-mod-group \
  +$(KAMPKG)-mod-htable +$(KAMPKG)-mod-kex +$(KAMPKG)-mod-lcr +$(KAMPKG)-mod-maxfwd \
  +$(KAMPKG)-mod-mediaproxy +$(KAMPKG)-mod-mi-datagram +$(KAMPKG)-mod-mi-fifo +$(KAMPKG)-mod-mi-rpc \
  +$(KAMPKG)-mod-msilo +$(KAMPKG)-mod-nat-traversal +$(KAMPKG)-mod-drouting +$(KAMPKG)-mod-xmlrpc \
  +$(KAMPKG)-mod-path +$(KAMPKG)-mod-presence \
  +$(KAMPKG)-mod-pv +$(KAMPKG)-mod-qos +$(KAMPKG)-mod-ratelimit +$(KAMPKG)-mod-regex +$(KAMPKG)-mod-registrar \
  +$(KAMPKG)-mod-rls +$(KAMPKG)-mod-rtimer +$(KAMPKG)-mod-sanity \
  +$(KAMPKG)-mod-siputils +$(KAMPKG)-mod-sl +$(KAMPKG)-mod-textops +$(KAMPKG)-mod-tls \
  +$(KAMPKG)-mod-tmx +$(KAMPKG)-mod-userblacklist +$(KAMPKG)-mod-utils \
  +$(KAMPKG)-mod-xcap-client +$(KAMPKG)-mod-xlog +$(KAMPKG)-mod-xmpp \
  +libssh2 +snort-legacy +snortsam \
  +coreutils +coreutils-nohup \
  +unixodbc \
  +luci-ssl \
  +kmod-fs-vfat

ifneq ($(ASTPKG),)
 ifeq ($(OWRT_NAME),trunk)
	BEESIP_DEPS += \
		+$(ASTPKG)-gui \
		+$(ASTPKG)-app-chanisavail +$(ASTPKG)-app-chanspy +$(ASTPKG)-app-exec +$(ASTPKG)-app-minivm \
		+$(ASTPKG)-app-originate +$(ASTPKG)-app-read +$(ASTPKG)-app-readexten +$(ASTPKG)-app-sayunixtime \
		+$(ASTPKG)-app-stack +$(ASTPKG)-app-system \
		+$(ASTPKG)-app-talkdetect +$(ASTPKG)-app-verbose +$(ASTPKG)-app-waituntil +$(ASTPKG)-app-while \
		+$(ASTPKG)-chan-iax2 +$(ASTPKG)-chan-dongle +$(ASTPKG)-codec-a-mu \
		+$(ASTPKG)-codec-alaw +$(ASTPKG)-codec-g722 +$(ASTPKG)-codec-g726 +$(ASTPKG)-codec-gsm \
		+$(ASTPKG)-curl \
		+$(ASTPKG)-format-g726 +$(ASTPKG)-format-gsm \
		+$(ASTPKG)-format-g729 +$(ASTPKG)-format-sln +$(ASTPKG)-func-blacklist \
		+$(ASTPKG)-func-channel +$(ASTPKG)-func-cut +$(ASTPKG)-func-db +$(ASTPKG)-func-devstate +$(ASTPKG)-func-extstate \
		+$(ASTPKG)-func-global +$(ASTPKG)-func-shell +$(ASTPKG)-func-uri +$(ASTPKG)-func-vmcount \
		+$(ASTPKG)-pbx-ael +$(ASTPKG)-pbx-spool +$(ASTPKG)-res-ael-share +$(ASTPKG)-res-agi \
		+$(ASTPKG)-res-musiconhold +$(ASTPKG)-cdr +$(ASTPKG)-sounds +$(ASTPKG)-voicemail +$(ASTPKG)-cdr-sqlite3 +$(ASTPKG)-cdr-csv \
		+$(ASTPKG)-chan-ooh323
 else ifeq ($(OWRT_NAME),barrier_breaker)
	BEESIP_DEPS += \
		+$(ASTPKG)-gui \
		+$(ASTPKG)-app-chanisavail +$(ASTPKG)-app-chanspy +$(ASTPKG)-app-exec +$(ASTPKG)-app-minivm \
		+$(ASTPKG)-app-originate +$(ASTPKG)-app-read +$(ASTPKG)-app-readexten +$(ASTPKG)-app-sayunixtime \
		+$(ASTPKG)-app-sms +$(ASTPKG)-app-stack +$(ASTPKG)-app-system \
		+$(ASTPKG)-app-talkdetect +$(ASTPKG)-app-verbose +$(ASTPKG)-app-waituntil +$(ASTPKG)-app-while \
		+$(ASTPKG)-chan-iax2 +$(ASTPKG)-chan-dongle +$(ASTPKG)-codec-a-mu \
		+$(ASTPKG)-codec-alaw +$(ASTPKG)-codec-g722 +$(ASTPKG)-codec-gsm +$(ASTPKG)-codec-g726 +$(ASTPKG)-curl +$(ASTPKG)-format-g726 \
		+$(ASTPKG)-format-g729 +$(ASTPKG)-format-sln +$(ASTPKG)-format-gsm +$(ASTPKG)-func-blacklist \
		+$(ASTPKG)-func-channel +$(ASTPKG)-func-cut +$(ASTPKG)-func-db +$(ASTPKG)-func-devstate +$(ASTPKG)-func-extstate \
		+$(ASTPKG)-func-global +$(ASTPKG)-func-shell +$(ASTPKG)-func-uri +$(ASTPKG)-func-vmcount \
		+$(ASTPKG)-pbx-ael +$(ASTPKG)-pbx-spool +$(ASTPKG)-res-ael-share +$(ASTPKG)-res-agi \
		+$(ASTPKG)-res-musiconhold +$(ASTPKG)-cdr +$(ASTPKG)-sounds +$(ASTPKG)-voicemail +$(ASTPKG)-chan-agent \
		+$(ASTPKG)-chan-ooh323
 else
	BEESIP_DEPS += \
		+$(ASTPKG)-gui +$(ASTPKG)-app-chanisavail +$(ASTPKG)-app-chanspy +$(ASTPKG)-app-exec +$(ASTPKG)-app-minivm \
		+$(ASTPKG)-app-originate +$(ASTPKG)-app-read +$(ASTPKG)-app-readexten +$(ASTPKG)-app-sayunixtime +$(ASTPKG)-app-sms \
		+$(ASTPKG)-app-stack +$(ASTPKG)-app-system +$(ASTPKG)-app-talkdetect +$(ASTPKG)-app-verbose +$(ASTPKG)-app-waituntil \
		+$(ASTPKG)-app-while +$(ASTPKG)-chan-iax2 +$(ASTPKG)-chan-dongle +$(ASTPKG)-codec-a-mu +$(ASTPKG)-codec-alaw +$(ASTPKG)-codec-g722 \
		+$(ASTPKG)-codec-g726 +$(ASTPKG)-curl +$(ASTPKG)-format-g726 +$(ASTPKG)-format-g729 +$(ASTPKG)-format-sln \
		+$(ASTPKG)-func-blacklist +$(ASTPKG)-func-channel +$(ASTPKG)-func-cut +$(ASTPKG)-func-db +$(ASTPKG)-func-devstate \
		+$(ASTPKG)-func-extstate +$(ASTPKG)-func-global +$(ASTPKG)-func-shell +$(ASTPKG)-func-uri +$(ASTPKG)-func-vmcount \
		+$(ASTPKG)-pbx-ael +$(ASTPKG)-pbx-spool +$(ASTPKG)-res-ael-share +$(ASTPKG)-res-agi +$(ASTPKG)-res-musiconhold \
		+$(ASTPKG)-cdr +$(ASTPKG)-sounds +$(ASTPKG)-voicemail +$(ASTPKG)-chan-agent
 endif
endif


ifeq ($(TARGET_ARCH),x86)
  BEESIP_DEPS += +tshark
endif

BEESIP_HUGE_DEPS += \
  +$(KAMPKG)-mod-auth-db +$(KAMPKG)-mod-avpops +$(KAMPKG)-mod-benchmark +$(KAMPKG)-mod-cfg-db \
  +$(KAMPKG)-mod-db-mysql +$(KAMPKG)-mod-db-unixodbc +$(KAMPKG)-mod-dispatcher +$(KAMPKG)-mod-domainpolicy \
  +$(KAMPKG)-mod-h350 +$(KAMPKG)-mod-ldap +$(KAMPKG)-mod-pdt +$(KAMPKG)-mod-permissions \
  +$(KAMPKG)-mod-presence-dialoginfo +$(KAMPKG)-mod-presence-mwi +$(KAMPKG)-mod-presence-xml \
  +$(KAMPKG)-mod-sms +$(KAMPKG)-mod-speeddial +$(KAMPKG)-mod-sqlops +$(KAMPKG)-mod-sst \
  +$(KAMPKG)-mod-statistics +$(KAMPKG)-mod-uac +$(KAMPKG)-mod-uac-redirect +$(KAMPKG)-mod-uri-db \
  +php5 +php5-cgi +php5-cli +php5-fastcgi +php5-mod-ctype \
  +php5-mod-curl +php5-mod-dom +php5-mod-exif +php5-mod-fileinfo +php5-mod-ftp \
  +php5-mod-gmp +php5-mod-hash +php5-mod-iconv +php5-mod-json \
  +php5-mod-ldap +php5-mod-mbstring +php5-mod-mcrypt +php5-mod-openssl \
  +php5-mod-pcntl +php5-mod-pdo +php5-mod-pdo-sqlite +php5-mod-session \
  +php5-mod-simplexml +php5-mod-soap +php5-mod-sockets \
  +php5-mod-sqlite3 +php5-mod-sysvmsg +php5-mod-sysvsem +php5-mod-sysvshm \
  +php5-mod-tokenizer +php5-mod-xml +php5-mod-xmlreader +php5-mod-xmlwriter \
  +unixodbc-tools \
  +bzip2 +ntpdate \
  +jamvm +classpath \
  +ip
