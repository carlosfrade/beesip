BEESIPPKG=y
BEESIPPKG_LITE=y
BEESIPPKG_HUGE=y
BEESIPPKG_DEV=n

BEESIP_NODEFAULTS=generic

BEESIP_PACKAGES += \
	uciprov=y

UCIPROV_UCIDEFAULTS += system.@system[0].hostname=Beesip-PBX
UCIPROV_OPKG +=  "update" "install uciprov"

OWRT_ADDED_FILES += $(call AddDirFiles,pbx-files/www) $(call AddDirFiles,pbx-files/etc)
