# Beesip package #

Beesip package is used to glue all needed VoIP software together. It has some common operating modes. One of them is SBC.

## Beesip as SBC ##

Session Border Controller is used for security reasons to make internal ifrastructure safe. Beesip helps to implement it by opensource technologies
and tries to make it as easy as possible using higher level uci configuration.

Example /etc/config/sbc with documentation
```

      # Global configuration
config global
      option enable		1
	# If disabled, beesip will not change any config files of asterisk/kamailio

      # Interface config
config interface
      option name sipudp-external
      option protocol sip
      option transport udp
      option ip 192.168.1.1
      option port 5060

config interface
      option name siptcp-internal
      option protocol sip
      option transport tcp
      option ip 192.168.5.1
      option port 5060

config interface
      option name siptls-external
      option protocol sip
      option transport tls
      option ip 192.168.1.1
      option port 5061

      #Theoreticaly possible another protocol. Focus on SIP now
config interface
      option name h323
      option protocol h323
      
      # Zone configuration
config zone 
      option name internal
      
      # Interfaces are inside zone
      list interface siptcp-internal
      # default policy for outbound calls from this zone
      option default_outbound drop
      # default policy for inbound calls to this zone
      option default_inbound drop

config zone 
      option name external
      
      # Interfaces are inside zone
      list interface sipudp-external
      list interface siptls-external
      
      # Which methods are allowed to zone
      option allow-methods REGISTER,INVITE,OPTIONS

config prefixgroup
	option name internal
	list prefix 420xxxxxxxxx
	list prefix 420yyyyyyyyy

config prefixgroup
	option name redirect1
	list prefix 420rrrrrrrrr

config prefixgroup
	option name cesnet
	list prefix 420ccccccccc

config gateway
	option name pstngw
	option zone internal
	option host 1.2.3.4
	#option port 5060
	#option transport udp
	#option fromdomain 'acme.com'

config rule
      # Match if request arrived at internal zone
      option srczone internal
      # Only authenticated users 
      option authenticated 1
      # Source user part. If not used, all users allowed
      list srcuser 'bimbo'
      # Source domain part. If not used, all domains allowed
      list srcdomain 'acme.net'
      # Which zone to route requests to
      option dstzone external
      # If enabled, all user part requests will be rewrited to this
      #option dstuser 1234
      # --//-- domain to this
      #option dstdomain acme.com
      # Use b2bua for this rule 
      option target b2bua

config rule
      # Match if request arrived to internal numbers
      option dstzone internal
      option dstprefix internal
      option target b2bua

config rule
      # Match if request arrived from known gw to internal numbers
      option srcgw pstngw
      option dstzone internal
      option dstprefix internal
      option target b2bua

config rule
      # Allow only local numbers to call outside
      option srczone internal
      option dstzone external
      option srcprefix internal
      option target b2bua
      # Force calls to right gw
      option dstgw pstngw

config rule
      # Allow only local numbers to call outside
      option srczone internal
      option dstzone external
      option target block

config rule
      # Send redirect message
      option srczone internal
      option dstprefix redirect1
      option target redirect
      option redirect_uri 'udp:1.2.3.4:5060'

config rule
      # Allow everything within internal zone
      option srczone internal
      option dstzone internal
      option target pass

      
```