#
# Copyright (C) 2013 Liptel team
#
# This is free software, licensed under the GNU General Public License v2.
#

menu "Configuration"
	depends on PACKAGE_uciprov

	menu "Provisioning URI configuration"
			config UCIPROV_USE_DNS
				bool "DNS"

			config UCIPROV_USE_DNSSEC
				bool "DNSSEC"

			config UCIPROV_USE_DHCP
				bool "DHCP"

			config UCIPROV_USE_LLDP
				depends on BROKEN
				bool "LLDP"

			config UCIPROV_USE_STATIC
				bool "STATIC"
				
			config UCIPROV_USE_ZABBIX
				bool "ZABBIX"
			
		config UCIPROV_URI1
			string "Static provisioning URI 1"
			depends on UCIPROV_USE_STATIC
			default "http://uciprov.{domain}/{board}/{cn}/{m}.{fde}"
		config UCIPROV_URI2
			string "Static provisioning URI 2"
			depends on UCIPROV_USE_STATIC
			default "http://uciprov.{domain}/{board}/{cn}/{m}/{ip}.{fde}"
		config UCIPROV_URI3
			string "Static provisioning URI 3"
			depends on UCIPROV_USE_STATIC
			default "http://uciprov.{domain}/{board}/{cn}/{m}/{mac}.{fde}"
		config UCIPROV_DNS_PREFIX
			string "DNS provisioning prefix" 
			depends on UCIPROV_USE_DNS||UCIPROV_USE_DNSSEC
			default "beesip"
		
	endmenu

	config UCIPROV_INTERFACE
			string "Provisioning URI interface"
			default "wan"

	config UCIPROV_TGZ
			bool "Tgz deployment module"
			default n

	config UCIPROV_SU
			bool "Sysupgrade module"
			default n
			
	config UCIPROV_ZABBIX
			bool "Zabbix module"
			default n
			
	config UCIPROV_RECOVERY
			bool "Recovery module"
			default n
			
	config UCIPROV_OPKG
			bool "Opkg module"
			default n
			
	config UCIPROV_SSLDEPLOY
			bool "SSL Deploy module"
			default n
	
	config UCIPROV_SERVICES
			bool "Services module"
			default n
	
	config UCIPROV_MGMT
			bool "Management module"
			default n
			
	config UCIPROV_DEBUG
			string "Uciprov default debug level"
			default 0
	
	menu "Tgz module configuration"
			depends on UCIPROV_TGZ
			
			config UCIPROV_TGZ_ONLYFD
			bool "Apply only when in factory defaults"
			default y

			config UCIPROV_TGZURI
			string "Static URI for .tgz deployment"
			depends on UCIPROV_TGZ && UCIPROV_USE_STATIC
			default ""
	endmenu

	menu "Sysupgrade module configuration"
			depends on UCIPROV_SU

			config UCIPROV_SUURI
			string "Static URI for sysupgrade"
			depends on UCIPROV_SU && UCIPROV_USE_STATIC
			default ""

			config UCIPROV_SU_ONLYFD
			bool "Apply only when in factory defaults"
			default y
	endmenu

	menu "Generic configuration"
		config UCIPROV_REBOOT_AFTER_CFG_APPLY
			bool "Reboot after CFG is applied"
			default n

		config UCIPROV_DELAY_BEFORE_REBOOT
			string "Delay before reboot"
			default "180"

		config UCIPROV_AUTOSTART
			bool "Autostart enabled"
			default y
		
		config UCIPROV_REPEAT
			string "If set to other number than 0, repeat uci process this number of seconds"
			default "0"
			help
			  Number of seconds to repeat uciprov process. If set to zero, no retrying will be done.
			  Can be used to repeatly check uci uri to update device in periodic intervals. 

		config UCIPROV_REPEAT_ON_SUCCESS
			bool "If set, uciprov will continue to fetch periodicaly even when uci configuration was successful"
			default n
			help
			  When uciprov fetches data from network and this option is set to true, uciprov will continue 
			  periodicaly fetching the same URI addresses.
			  If this option is set to n, uciprov will stop retrying after successful provision.
		
	endmenu
	
	menu "Zabbix module configuration"
		depends on UCIPROV_ZABBIX

		config UCIPROV_ZABBIX_APIURI
			string "Zabbix API URI"
			default "http://zabbix.{domain}/api_jsonrpc.php"

		config UCIPROV_ZABBIX_APIUSER
			string "Zabbix API user"
			default "uciprov"

		config UCIPROV_ZABBIX_APIPW
			string "Zabbix API password"
			default "uciprovpw"
	endmenu
	
	menu "Services module configuration"
		depends on UCIPROV_SERVICES

		config UCIPROV_SERVICES_URI
			string "Services control file uri"
			default ""
	endmenu

	menu "Opkg module configuration"
		depends on UCIPROV_OPKG

		config UCIPROV_OPKG_URI
			string "Opkg control file uri"
			default ""
	endmenu
	
	menu "SSLDeploy module configuration"
		depends on UCIPROV_SSLDEPLOY

		config UCIPROV_SSLDEPLOY_KEYFILE
			string "SSLDeploy key storage file"
			default "/etc/ssl/box.key.pem"
			
		config UCIPROV_SSLDEPLOY_CRTFILE
			string "SSLDeploy certificate storage file"
			default "/etc/ssl/box.crt.pem"	

		config UCIPROV_SSLDEPLOY_CSRFILE
			string "SSLDeploy csr storage file"
			default "/etc/ssl/box.csr.pem"
			
		config UCIPROV_SSLDEPLOY_PUSHURI
			string "SSLDeploy csr push URI"
			default "http://uciprov.{domain}/{m}/{mac}.csr"
			
		config UCIPROV_SSLDEPLOY_FETCHURI
			string "SSLDeploy crt fetch URI"
			default "http://uciprov.{domain}/{m}/{mac}.crt"
	endmenu
	
	menu "Management module configuration"
		depends on UCIPROV_MGMT

		config UCIPROV_MGMT_SETROOTPW
			bool "Set random root password"
			default 0
			
		config UCIPROV_MGMT_AUTHKEYSFILE
			string "Authorized keys file"
			default "/etc/dropbear/authorized_keys"
		
		config UCIPROV_MGMT_DISABLE_SSHPWAUTH
			bool "Disable SSH password authentication in dropbear"
			default 0
		
		config UCIPROV_MGMT_SAVEROOTPW
			string "Save random root password to file. Skip if empty string"
			default "/etc/rootpw"
		
		config UCIPROV_MGMTURI
			string "Mgmt uri"
			default ""
		
	endmenu
	
	menu "Recovery module configuration"
		depends on UCIPROV_RECOVERY

		config UCIPROV_RECOVERY_FDURI
			string "Recovery uri from which to get flag"
			default ""

		config UCIPROV_RECOVERY_FDFLAG
			string "If factory defaults url contains this string, box will be resetet to factory defaults."
			default "1234"
			
		config UCIPROV_RECOVERY_DHCP
			bool "If everything fails, try to get boot_file uri from DHCP and do actions"
			default y
			help
			  It will check, if boot_file end with "recovery.img". If so, it will do sysupgrade.
			  It will also check if boot_file end with "uci.default". If so, it will import uci and reboots.
			  It will also check if boot_file starts with "shell". If so, it will run shell script.	

		config UCIPROV_RECOVERY_SH
			string "Shell script to run when in recovery mode."
			default ""
   
	endmenu

endmenu

