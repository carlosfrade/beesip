$(eval $(call BeesipDefaults,x86_32))
$(eval $(call BeesipDefaults,virtual))
$(eval $(call BeesipDefaults,sbc))

TARGET_CPU=x86
OWRT_NAME=chaos_calmer
TARGET_NAME=pbx_$(BEESIP_VERSION)-virtual_$(TARGET_CPU)
OWRT_IMG_PROFILE=Generic
BEESIP_PACKAGES +=
OWRT_CONFIG_SET += TARGET_x86=y TARGET_BOARD=\"x86\" HAS_SUBTARGETS=y
