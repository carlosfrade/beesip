# Target for Alix x86 host
# This target should work with virtualization software like vmware, kvm or VirtualBox
# Can be run directly on some x86 hardware, but it depends on hardware

# Target CPU - mandatory settings

TARGET_CPU=x86

# OpenWrt used for compilation (trunk, attitude_adjustment, ...)

OWRT_NAME=chaos_calmer

# Will be used as a prefix for various images

TARGET_NAME=alix_$(BEESIP_VERSION)-$(TARGET_CPU)

# Options for qemu emulation of target

TARGET_QEMU=i386
TARGET_QEMU_OPTS=-m 512

# Kamailio package used for build (kamailio3 or kamailio4)

KAMPKG=kamailio4

$(eval $(call BeesipDefaults,x86))

OWRT_IMG_DISK_NAME=openwrt-$(OWRT_NAME)-$(TARGET_CPU)-geode-combined-squashfs.img
OWRT_IMG_KERNEL_NAME=openwrt-$(OWRT_NAME)-$(TARGET_CPU)-geode-vmlinuz
OWRT_IMG_SQUASHFS_NAME=openwrt-$(OWRT_NAME)-$(TARGET_CPU)-geode-rootfs-squashfs.img
OWRT_IMG_PROFILE=Generic

BEESIPPKG=y
BEESIPPKG_LITE=y
BEESIPPKG_HUGE=y
BEESIPPKG_DEV=m

OWRT_CONFIG_SET += TARGET_x86=y TARGET_BOARD=\"x86\" TARGET_x86_geode=y TARGET_x86_geode_Default=y HAS_SUBTARGETS=y TARGET_KERNEL_PARTSIZE=30 TARGET_ROOTFS_PARTSIZE=300 TARGET_OVERLAY_JFFS2=y
OWRT_CONFIG_UNSET += TARGET_IMAGES_GZIP
