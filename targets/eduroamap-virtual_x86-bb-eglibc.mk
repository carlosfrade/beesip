
include targets/virtual-x86-barrier_breaker.mk

# EGLIBC options
EGLIBC=y
OWRT_PATCHES += eglibc-bb.patch

TARGET_CPU_SUFFIX = -eglibc
OWRT_CONFIG_SET += TOOLCHAINOPTS=y LIBC_USE_EGLIBC=y
OWRT_CONFIG_UNSET += PACKAGE_mysql-server PACKAGE_libmysqlclient PACKAGE_libmysqlclient-r PACKAGE_cbtt-mysql PACKAGE_imspector PACKAGE_lcd4linux-custom PACKAGE_lcd4linux-full PACKAGE_libdbi-drivers-mysql PACKAGE_luasql-mysql PACKAGE_nfacctd-mysql PACKAGE_pmacctd-mysql PACKAGE_sfacctd-mysql PACKAGE_uacctd-mysql PACKAGE_nprobe PACKAGE_php5-mod-mysql PACKAGE_php5-mod-mysqli PACKAGE_php5-mod-pdo-mysql PACKAGE_python-mysql PACKAGE_snort-mysql PACKAGE_snort-wireless-mysql PACKAGE_strongswan-mod-mysql PACKAGE_ulogd-mod-mysql PACKAGE_asterisk18-mysql PACKAGE_asterisk11-mysql PACKAGE_kamailio3-mod-db-mysql PACKAGE_kamailio4-mod-db-mysql PACKAGE_restund-mod-mysql PACKAGE_yate-mod-mysqldb PACKAGE_myodbc 
BEESIP_PACKAGES += libmysqlclient=n asterisk18-mysql=N asterisk11-mysql=N kamailio3-mod-db-mysql=N kamailio4-mod-db-mysql=N 
