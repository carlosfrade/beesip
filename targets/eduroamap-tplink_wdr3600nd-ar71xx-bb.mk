TARGET_CPU=ar71xx
OWRT_NAME=barrier_breaker

# HWREV can be added as commandline argument to make
ifeq ($(HWREV),)
HWREV=wdr3600-v1
endif

TARGET_NAME=eduroamap_$(HWREV)_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

$(eval $(call BeesipDefaults,eduroam))

OWRT_IMG_BIN_NAME=openwrt-ar71xx-generic-tl-$(HWREV)-squashfs-factory.bin
OWRT_IMG_KERNEL_NAME=openwrt-$(TARGET_CPU)-generic-vmlinux.elf
OWRT_IMG_PROFILE=TLWDR4300

BEESIP_PACKAGES += kmod-usb-core=y kmod-usb-storage=y kmod-fs-vfat=y kmod-fs-ext4=y block-mount=y

OWRT_CONFIG_SET += TARGET_ar71xx=y TARGET_ar71xx_generic_TLWDR4300=y 
